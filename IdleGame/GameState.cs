﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace IdleGame
{
    internal class GameState
    {
        public int manualClicks { get; set; }
        public double currentPoints { get; set; }
        public double pointsSpent { get; set; }
        public double totalPoints { get; set; }


        // Game Cycle Variables
        public double cycleTime { get; set; } = 5;
        public double pointsPerCycle { get; set; } = 0;

        // Power Ups
        public bool GoblinGrunt { get; set; } = false;
        public double GoblinGruntMultiplier { get; set; } = 0.01;
        public int GoblinGruntPurchaseCost { get; set; } = 100;
        public bool GoblinBrute { get; set; } = false;
        public double GoblinBruteMultiplier { get; set; } = 0.2;
        public int GoblinBrutePurchaseCost { get; set; } = 500;
        public bool GoblinKing { get; set; } = false;
        public double GoblinKingMultiplier { get; set; } = 1;
        public int GoblinKingPurchaseCost { get; set; } = 1000;

        // Shop Items
        public int autoClickers { get; set; } = 0;
        public double autoClickerMultiplier { get; set; } = 0.1;

        public double autoClickerCostPerUnitMultiplier = 1.15;
        public double autoClickerCostPerUnit { get; set; } = 10;

        public int farmers { get; set; } = 0;
        public double farmerMultiplier { get; set; } = 0.5;

        public double farmerCostPerUnitMultiplier = 1.35;
        public double farmerCostPerUnit { get; set; } = 20;

        public GameState()
        {
            manualClicks = 0;
            currentPoints = 0;
            pointsSpent = 0;
            autoClickers = 0;
            autoClickerMultiplier = 0.1;
        }

    }
}
