﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace IdleGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        GameState gameState = new GameState();
        private static System.Timers.Timer ccTimer;
        private static System.Timers.Timer pbTimer;
        public MainWindow()
        {
            InitializeComponent();
            RawCycleTime.Content = gameState.cycleTime;


            // Cycle Clicking Timer
            ccTimer = new System.Timers.Timer();
            ccTimer.Interval = TimeSpan.FromSeconds(gameState.cycleTime).TotalMilliseconds;
            ccTimer.Elapsed += ccTimer_Elapsed;
            ccTimer.AutoReset = true;

            pbTimer = new System.Timers.Timer();
            pbTimer.Interval = TimeSpan.FromSeconds(gameState.cycleTime).TotalMilliseconds / 5;
            pbTimer.Elapsed += PbTimer_Elapsed;
            pbTimer.AutoReset = true;
        }

        private void ccTimer_Elapsed(object? sender, ElapsedEventArgs e)
        {
            UpdateGameStateAddCycleClicks();
        }

        private void PbTimer_Elapsed(object? sender, ElapsedEventArgs e)
        {
            AdvanceProgressBar();
        }

        private void AdvanceProgressBar()
        {
            
            Dispatcher.Invoke(() =>
            {
                if (CycleTimeBar.Value < 100) { CycleTimeBar.Value += 20; }
                else { CycleTimeBar.Value = 20; }
            });
            
        }
        private void ManualClick(object sender, RoutedEventArgs e)
        {
            UpdateGameStateAddManClick();
        }

        private void UpdateGameStateAddManClick()
        {
            gameState.manualClicks++;
            gameState.currentPoints++;
            gameState.totalPoints++;
            UpdatePoints();
        }

        private void UpdateGameStateAddCycleClicks()
        {
            Dispatcher.Invoke(() =>
            {
                gameState.currentPoints += gameState.pointsPerCycle;
                gameState.totalPoints += gameState.pointsPerCycle;
                UpdatePoints();
            }); 
        }

        private void UpdateCycle()
        {
            ccTimer.Stop();
            pbTimer.Stop();
            ccTimer.Start();
            pbTimer.Start();
            CycleTimeBar.Value = 20;
            ccTimer.Interval = TimeSpan.FromSeconds(gameState.cycleTime).TotalMilliseconds;
            pbTimer.Interval = TimeSpan.FromSeconds(gameState.cycleTime).TotalMilliseconds / 5;
            RawCycleTime.Content = gameState.cycleTime;
            UpdatePointsPerCycle();
            if (pbTimer.Enabled) CycleTimeLabel.Content = "Cycle Time: " + String.Format("{0:0.0}", gameState.cycleTime) + "s";
        }

        private void UpdateCycleTime()
        {
            gameState.cycleTime -= Convert.ToInt32(gameState.GoblinGrunt) * gameState.GoblinGruntMultiplier;
            gameState.cycleTime -= Convert.ToInt32(gameState.GoblinBrute) * gameState.GoblinBruteMultiplier;
            gameState.cycleTime -= Convert.ToInt32(gameState.GoblinKing) * gameState.GoblinKingMultiplier;
            UpdateCycle();
        }

        private void UpdatePointsPerCycle()
        {
            gameState.pointsPerCycle = 0;
            gameState.pointsPerCycle += gameState.autoClickers * gameState.autoClickerMultiplier;
            gameState.pointsPerCycle += gameState.farmers * gameState.farmerMultiplier;
            AutoClickersPPS.Content = GetPointsPerSecond(gameState.autoClickers * gameState.autoClickerMultiplier);
            FarmersPPS.Content = GetPointsPerSecond(gameState.farmers * gameState.farmerMultiplier);
            PointsPerSecondLabel.Content = "PPS: " + GetPointsPerSecond(gameState.pointsPerCycle);
        }

        private void UpdatePoints()
        {
            pointsLabel.Content = "Points: " + String.Format("{0:0.0}", gameState.currentPoints);
        }

        private string GetPointsPerSecond(double pointsPerCycle)
        {
            return String.Format("{0:0.00}", pointsPerCycle / gameState.cycleTime);
        }

        private void PurchaseAutoClicker(object sender, RoutedEventArgs e)
        {
            if(gameState.currentPoints >= gameState.autoClickerCostPerUnit)
            {
                gameState.autoClickers++;
                gameState.currentPoints -= gameState.autoClickerCostPerUnit;
                gameState.pointsSpent += gameState.autoClickerCostPerUnit;
                gameState.autoClickerCostPerUnit = gameState.autoClickerCostPerUnit * gameState.autoClickerCostPerUnitMultiplier;
                MessageCenterTextLabel.Content = "Purchased Auto Clicker";
                if (pbTimer.Enabled == false) { pbTimer.Enabled = true; }
                if (ccTimer.Enabled == false) { ccTimer.Enabled = true; UpdateCycle(); }
                PurchasedAutoClickers.Content = gameState.autoClickers;
                AutoClickerPurchaseCostLabel.Content = String.Format("{0:0.0}", gameState.autoClickerCostPerUnit);
                UpdatePoints();
                UpdatePointsPerCycle();
            } else { MessageCenterTextLabel.Content = "Not Enough Points"; }
        }

        private void PurchaseFarmer(object sender, RoutedEventArgs e)
        {
            if (gameState.currentPoints >= gameState.farmerCostPerUnit)
            {
                gameState.farmers++;
                gameState.currentPoints -= gameState.farmerCostPerUnit;
                gameState.pointsSpent += gameState.farmerCostPerUnit;
                gameState.farmerCostPerUnit = gameState.farmerCostPerUnit * gameState.farmerCostPerUnitMultiplier;
                MessageCenterTextLabel.Content = "Purchased Farmer";
                if (pbTimer.Enabled == false) { pbTimer.Enabled = true; }
                if (ccTimer.Enabled == false) { ccTimer.Enabled = true; UpdateCycle(); }
                PurchasedFarmers.Content = gameState.farmers;
                FarmerPurchaseCostLabel.Content = String.Format("{0:0.0}", gameState.farmerCostPerUnit);
                UpdatePoints();
                UpdatePointsPerCycle();
            } else { MessageCenterTextLabel.Content = "Not Enough Points"; }
        }

        private void PurchaseGoblinGrunt(object sender, RoutedEventArgs e)
        {
            if(gameState.currentPoints >= gameState.GoblinGruntPurchaseCost)
            {
                gameState.GoblinGrunt = true;
                gameState.currentPoints -= gameState.GoblinGruntPurchaseCost;
                PurchaseGoblinGruntButton.IsEnabled = false;
                UpdateCycleTime();
            } else { MessageCenterTextLabel.Content = "Not Enough Points"; }
        }

        private void PurchaseGoblinBrute(object sender, RoutedEventArgs e)
        {
            if (gameState.currentPoints >= gameState.GoblinBrutePurchaseCost)
            {
                gameState.GoblinBrute = true;
                gameState.currentPoints -= gameState.GoblinBrutePurchaseCost;
                PurchaseGoblinBruteButton.IsEnabled = false;
                UpdateCycleTime();
            }
            else { MessageCenterTextLabel.Content = "Not Enough Points"; }
        }

        private void PurchaseGoblinKing(object sender, RoutedEventArgs e)
        {
            if (gameState.currentPoints >= gameState.GoblinKingPurchaseCost)
            {
                gameState.GoblinKing = true;
                gameState.currentPoints -= gameState.GoblinKingPurchaseCost;
                PurchaseGoblinKingButton.IsEnabled = false;
                UpdateCycleTime();
            }
            else { MessageCenterTextLabel.Content = "Not Enough Points"; }
        }

        // DEVELOPER CHEATS
        private void CheatCycleTime(object sender, RoutedEventArgs e)
        {
            if (gameState.cycleTime > .5) gameState.cycleTime -= .5;
            else if (gameState.cycleTime > .06) gameState.cycleTime -= .05;
            else gameState.cycleTime = gameState.cycleTime;
            UpdateCycle();
        }

        private void CheatAdd500Points(object sender, RoutedEventArgs e)
        {
            gameState.currentPoints += 500;
            UpdatePoints();
        }
    }
}
